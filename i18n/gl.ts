<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Un cliente Jabber/XMPP simple e amigable</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licenza:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Ver código fonte en liña</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Copy Message</source>
        <translation>Copiar Mensaxe</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>Editar Mensaxe</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Redactar mensaxe</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Imaxe</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Son</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Documento</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>Outros ficheiros</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Escoller ficheiro</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Enviar mensaxe spoiler</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>Aviso Spoiler</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Non se gardou o ficheiro: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Fallou a descarga: %1</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Por favor, escolla unha conversa para comezar</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>Escoller ficheiro</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Go to parent folder</source>
        <translation>Ir ao cartafol superior</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Pechar</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Convidar amizades</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Ligazón do convite copiada</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Abrirase a ligazón despois de conectarse.</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Conectar coa súa conta XMPP</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>O seu ID-Jabber:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>usuaria@exemplo.org</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>Contrasinal:</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Conectando…</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Usuario ou contrasinal non válidos.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Non puido conectar co servidor. Por favor, comprobe a conexión a internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>O servidor non soporta conexións seguras.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Fallo ao intentar conectar de xeito seguro.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation>Non se atopou o enderezo do servidor. Comprobe o seu JID de novo.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>Non puido conectar co servidor.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Protocolo de autenticación non soportado polo servidor.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>Aconteceu un fallo descoñecido; vexa máis detalles no rexistro.</translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>Non se enviou a mensaxe, xa que non está conectada.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>Non se correxiu a mensaxe, xa que non está conectada.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Engadir novo contacto</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Esto tamén enviará unha solicitude  para acceder a presenza do contacto.</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>ID-Jabber:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>usuaria@exemplo.org</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Alcume:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Mensaxe optativa:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Dígalle ao seu contacto quen é vostede.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Engadir</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Fallo: comprobe o JID.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation>Dispoñible</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation>Libre para conversar</translation>
    </message>
    <message>
        <source>Away</source>
        <translation>Fóra</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation>Non molestar</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation>Fóra para longo</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Desconectada</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fallo</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Non se engadiu o contacto, xa que non está conectada.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Non se eliminou o contacto, xa que non está conectada.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Conectando…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Contactos</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Engadir novo contacto</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Desconectada</translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Desexa eliminar o contacto &lt;b&gt;%1&lt;/b&gt; da lista?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Eliminar contacto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation>Descrición</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Solicitude de subscrición</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Recibeu unha solicitude de subscrición de &lt;b&gt;%1&lt;/b&gt;. Se acepta, a conta terá acceso ao seu estado de presenza.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Rexeitar</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Non se enviou o ficheiro, xa que non está conectada.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Ficheiro</translation>
    </message>
</context>
</TS>
